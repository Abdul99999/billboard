package com.example.adstream;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.DynamicConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    SimpleExoPlayer player;
    DataSource.Factory dataSourceFactory;
    ExtractorsFactory extractorsFactory;
   private Context mContext = MainActivity.this;
    private DynamicConcatenatingMediaSource dynamicMediaSource;
    private ArrayList<String> uris;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY ;
        decorView.setSystemUiVisibility(uiOptions);


        setContentView(R.layout.activity_main);

         createPlayer();
        // Bind the player to the view.
        SimpleExoPlayerView simpleExoPlayerView = findViewById(R.id.player_view);
        simpleExoPlayerView.setPlayer(player);

        preparePlayer();
/*        Button add = findViewById(R.id.addMed);
add.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        MediaSource mediaSource  = new ExtractorMediaSource(Uri.parse("http://mirrors.standaloneinstaller.com/video-sample/dolbycanyon.mp4"),
                dataSourceFactory, extractorsFactory, null, null);
        dynamicMediaSource.addMediaSource(mediaSource);
    }
});*/
    }

    public void createPlayer(){
      //  Creating the player

        // 1. Create a default TrackSelector
            Handler mainHandler = new Handler();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
             player = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector);

    }



    public void preparePlayer(){
        //
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

   /*     // Produces DataSource instances through which media data is loaded.
   dataSourceFactory = new DefaultDataSourceFactory(mContext,
                Util.getUserAgent(mContext, "adstream"), bandwidthMeter);
*/
     extractorsFactory = new DefaultExtractorsFactory();
// This is the MediaSource representing the media to be played.
   uris= new ArrayList<>();

        uris.add("http://mirrors.standaloneinstaller.com/video-sample/small.mp4");
        uris.add(" http://mirrors.standaloneinstaller.com/video-sample/grb_2.mp4");

        ArrayList<MediaSource> sources = new ArrayList<>();

        for (int i=0;i< uris.size();i++)
        {
            MediaSource mediaSource  = new ExtractorMediaSource(Uri.parse(uris.get(i)),
                    new CacheDataSourceFactory(mContext, 100 * 1024 * 1024, 5 * 1024 * 1024), extractorsFactory, null, null);
            sources.add(mediaSource);
        }


        dynamicMediaSource = new DynamicConcatenatingMediaSource();
        dynamicMediaSource.addMediaSources(sources);
        LoopingMediaSource loop = new LoopingMediaSource(dynamicMediaSource, 3);
       // player.addListener(this);
        player.prepare(loop,true,true);
 /*       player.setPlayWhenReady(true);*/


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
    }
    /*    public void initMusicPlayer() {
        if ( !=null)//list of uri
        {

            MediaSource mediaSource;
            ArrayList<MediaSource> sources = new ArrayList<>();

            for (int i=0;i< songs.size();i++)
            {
                mediaSource = buildMediaSource(Uri.parse(song.getMusicUrl()));
                sources.add(mediaSource);
            }
            dynamicMediaSource = new DynamicConcatenatingMediaSource();
            dynamicMediaSource.addMediaSources(sources);
            player.addListener(this);
            player.prepare(dynamicMediaSource,true,true);
            if (currentPlayingSongIndex == -1)
            {
                player.setPlayWhenReady(false);
            }
            else
            {
                if (currentDuration != -1)
                {
                    player.seekTo(currentPlayingSongIndex,currentDuration);
                }
                else
                {
                    player.seekTo(currentPlayingSongIndex,0);
                }
                player.setPlayWhenReady(true);
                sendBroadcast(new Intent("PLAY"));
                showNotification();
            }
        }
    }*/

/*    public void addItemToPlaylist(MusicItem song,boolean shouldPlay)
    {
        songs = SingleTon.sharedInstance().getMusicItemsList();
        if (dynamicMediaSource!=null)
        {
            MediaSource mediaSource = buildMediaSource(Uri.parse());
            dynamicMediaSource.addMediaSource(mediaSource);
            if (shouldPlay)
            {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        player.seekTo(currentPlayingSongIndex,0);
                    }
                },500);
            }
        }
        else
        {
            initMusicPlayer();
        }
    }*/
}
